package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Group.GroupServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GroupService.GetASpecificGroupInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.GetASpecificGroupReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetASpecificGroupTests {

	@Test
	public void testOperationGetASpecificGroupBasicMapping()  {
		GroupServiceDefaultImpl serviceDefaultImpl = new GroupServiceDefaultImpl();
		GetASpecificGroupInputParametersDTO inputs = new GetASpecificGroupInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetASpecificGroupReturnDTO returnValue = serviceDefaultImpl.getaspecificgroup(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}