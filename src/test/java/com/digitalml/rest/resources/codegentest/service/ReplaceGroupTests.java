package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Group.GroupServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GroupService.ReplaceGroupInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.ReplaceGroupReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class ReplaceGroupTests {

	@Test
	public void testOperationReplaceGroupBasicMapping()  {
		GroupServiceDefaultImpl serviceDefaultImpl = new GroupServiceDefaultImpl();
		ReplaceGroupInputParametersDTO inputs = new ReplaceGroupInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		ReplaceGroupReturnDTO returnValue = serviceDefaultImpl.replacegroup(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}