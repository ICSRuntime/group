package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Group.GroupServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GroupService.FindGroupInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.FindGroupReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindGroupTests {

	@Test
	public void testOperationFindGroupBasicMapping()  {
		GroupServiceDefaultImpl serviceDefaultImpl = new GroupServiceDefaultImpl();
		FindGroupInputParametersDTO inputs = new FindGroupInputParametersDTO();
		inputs.setOffset(0);
		inputs.setPagesize(0);
		FindGroupReturnDTO returnValue = serviceDefaultImpl.findgroup(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}