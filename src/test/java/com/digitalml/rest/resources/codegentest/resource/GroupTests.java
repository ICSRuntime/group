package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class GroupTests {

	@Test
	public void testResourceInitialisation() {
		GroupResource resource = new GroupResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetASpecificGroupNoSecurity() {
		GroupResource resource = new GroupResource();
		resource.setSecurityContext(null);

		Response response = resource.getaspecificgroup(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationFindGroupNoSecurity() {
		GroupResource resource = new GroupResource();
		resource.setSecurityContext(null);

		Response response = resource.findgroup(0, 0);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateGroupNoSecurity() {
		GroupResource resource = new GroupResource();
		resource.setSecurityContext(null);

		Response response = resource.updategroup(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationReplaceGroupNoSecurity() {
		GroupResource resource = new GroupResource();
		resource.setSecurityContext(null);

		Response response = resource.replacegroup(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateGroupNoSecurity() {
		GroupResource resource = new GroupResource();
		resource.setSecurityContext(null);

		Response response = resource.creategroup(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationDeleteGroupNoSecurity() {
		GroupResource resource = new GroupResource();
		resource.setSecurityContext(null);

		Response response = resource.deletegroup(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}