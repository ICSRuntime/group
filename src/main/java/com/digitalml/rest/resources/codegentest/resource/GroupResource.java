package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.GroupService;
	
import com.digitalml.rest.resources.codegentest.service.GroupService.GetASpecificGroupReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.GetASpecificGroupReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.GetASpecificGroupInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.FindGroupReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.FindGroupReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.FindGroupInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.UpdateGroupReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.UpdateGroupReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.UpdateGroupInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.ReplaceGroupReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.ReplaceGroupReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.ReplaceGroupInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.CreateGroupReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.CreateGroupReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.CreateGroupInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.DeleteGroupReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.DeleteGroupReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GroupService.DeleteGroupInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Group
	 * Insurance group access and update management.
	 *
	 * @author admin
	 * @version 1.0
	 *
	 */
	
	@Path("http://digitalml.com/ICS/rest")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class GroupResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private GroupService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Group.GroupServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private GroupService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof GroupService)) {
			LOGGER.error(implementationClass + " is not an instance of " + GroupService.class.getName());
			return null;
		}

		return (GroupService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: getaspecificgroup
		Gets group details

	Non-functional requirements:
	*/
	
	@GET
	@Path("/group/{id}")
	public javax.ws.rs.core.Response getaspecificgroup(
		@PathParam("id")@NotEmpty String id) {

		GetASpecificGroupInputParametersDTO inputs = new GroupService.GetASpecificGroupInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			GetASpecificGroupReturnDTO returnValue = delegateService.getaspecificgroup(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findgroup
		Gets a collection of group details filtered 

	Non-functional requirements:
	*/
	
	@GET
	@Path("/group")
	public javax.ws.rs.core.Response findgroup(
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindGroupInputParametersDTO inputs = new GroupService.FindGroupInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindGroupReturnDTO returnValue = delegateService.findgroup(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updategroup
		Updates group

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/group/{id}")
	public javax.ws.rs.core.Response updategroup(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		UpdateGroupInputParametersDTO inputs = new GroupService.UpdateGroupInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			UpdateGroupReturnDTO returnValue = delegateService.updategroup(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: replacegroup
		Replaces group

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/group/{id}")
	public javax.ws.rs.core.Response replacegroup(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		ReplaceGroupInputParametersDTO inputs = new GroupService.ReplaceGroupInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			ReplaceGroupReturnDTO returnValue = delegateService.replacegroup(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: creategroup
		Creates group

	Non-functional requirements:
	*/
	
	@POST
	@Path("/group")
	public javax.ws.rs.core.Response creategroup(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		CreateGroupInputParametersDTO inputs = new GroupService.CreateGroupInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			CreateGroupReturnDTO returnValue = delegateService.creategroup(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deletegroup
		Deletes group

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/group/{id}")
	public javax.ws.rs.core.Response deletegroup(
		@PathParam("id")@NotEmpty String id) {

		DeleteGroupInputParametersDTO inputs = new GroupService.DeleteGroupInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			DeleteGroupReturnDTO returnValue = delegateService.deletegroup(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}