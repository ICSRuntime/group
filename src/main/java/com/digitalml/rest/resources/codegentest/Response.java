package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Response:
{
  "required": [
    "group"
  ],
  "type": "object",
  "properties": {
    "group": {
      "$ref": "Group"
    }
  }
}
*/

public class Response {

	@Size(max=1)
	@NotNull
	private com.digitalml.insurance.group.Group group;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    group = new com.digitalml.insurance.group.Group();
	}
	public com.digitalml.insurance.group.Group getGroup() {
		return group;
	}
	
	public void setGroup(com.digitalml.insurance.group.Group group) {
		this.group = group;
	}
}