package com.digitalml.insurance.group;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Group:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "groupCode": {
      "type": "string"
    },
    "groupDescription": {
      "type": "string"
    },
    "fullName": {
      "type": "string"
    },
    "groupStatus": {
      "type": "string"
    },
    "numberMembers": {
      "type": "string"
    },
    "effectiveDate": {
      "type": "string"
    },
    "terminationDate": {
      "type": "string"
    },
    "duesAmount": {
      "description": "On Grouping, Dues is used with DuesMode to specify the amount of dues that must be paid per mode in order to maintain membership in the group. ",
      "type": "string"
    },
    "duesMode": {
      "type": "string"
    },
    "attachment": {
      "type": "array",
      "items": {
        "$ref": "Attachment"
      }
    },
    "address": {
      "type": "array",
      "items": {
        "$ref": "Address"
      }
    }
  }
}
*/

public class Group {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String groupCode;

	@Size(max=1)
	private String groupDescription;

	@Size(max=1)
	private String fullName;

	@Size(max=1)
	private String groupStatus;

	@Size(max=1)
	private String numberMembers;

	@Size(max=1)
	private String effectiveDate;

	@Size(max=1)
	private String terminationDate;

	@Size(max=1)
	private String duesAmount;

	@Size(max=1)
	private String duesMode;

	@Size(max=1)
	private List<com.digitalml.insurance.attachment.Attachment> attachment;

	@Size(max=1)
	private List<com.digitalml.insurance.common.contact.Address> address;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    groupCode = null;
	    groupDescription = null;
	    fullName = null;
	    groupStatus = null;
	    numberMembers = null;
	    effectiveDate = null;
	    terminationDate = null;
	    duesAmount = null;
	    duesMode = null;
	    attachment = new ArrayList<com.digitalml.insurance.attachment.Attachment>();
	    address = new ArrayList<com.digitalml.insurance.common.contact.Address>();
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupCode() {
		return groupCode;
	}
	
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getGroupDescription() {
		return groupDescription;
	}
	
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGroupStatus() {
		return groupStatus;
	}
	
	public void setGroupStatus(String groupStatus) {
		this.groupStatus = groupStatus;
	}
	public String getNumberMembers() {
		return numberMembers;
	}
	
	public void setNumberMembers(String numberMembers) {
		this.numberMembers = numberMembers;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getTerminationDate() {
		return terminationDate;
	}
	
	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}
	public String getDuesAmount() {
		return duesAmount;
	}
	
	public void setDuesAmount(String duesAmount) {
		this.duesAmount = duesAmount;
	}
	public String getDuesMode() {
		return duesMode;
	}
	
	public void setDuesMode(String duesMode) {
		this.duesMode = duesMode;
	}
	public List<com.digitalml.insurance.attachment.Attachment> getAttachment() {
		return attachment;
	}
	
	public void setAttachment(List<com.digitalml.insurance.attachment.Attachment> attachment) {
		this.attachment = attachment;
	}
	public List<com.digitalml.insurance.common.contact.Address> getAddress() {
		return address;
	}
	
	public void setAddress(List<com.digitalml.insurance.common.contact.Address> address) {
		this.address = address;
	}
}