package com.digitalml.insurance.common.contact;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Address:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "addressTypeCode": {
      "enum": [
        "Residence",
        "Previous",
        "Primary Address",
        "Previous Residence Address",
        "Previous Business Address",
        "Policy Mailing Address",
        "Mailing",
        "Business",
        "Billing Mailing"
      ],
      "type": "string"
    },
    "addressLine1": {
      "type": "string"
    },
    "addressLine2": {
      "type": "string"
    },
    "addressLine3": {
      "type": "string"
    },
    "addressLine4": {
      "type": "string"
    },
    "addressLine5": {
      "type": "string"
    },
    "city": {
      "type": "string"
    },
    "addressCountry": {
      "type": "string"
    },
    "addressState": {
      "type": "string"
    },
    "zip": {
      "type": "string"
    },
    "prefAddressInd": {
      "type": "string"
    },
    "startDate": {
      "type": "string"
    },
    "endDate": {
      "type": "string"
    },
    "lastUpdate": {
      "type": "string"
    },
    "yearsAtAddress": {
      "type": "string"
    },
    "returnedMailInd": {
      "type": "boolean"
    },
    "postalDropCode": {
      "type": "string"
    },
    "addressValidInd": {
      "type": "boolean"
    },
    "countyName": {
      "type": "string"
    },
    "countyCode": {
      "type": "string"
    },
    "updateInd": {
      "type": "boolean"
    },
    "uspsValidInd": {
      "type": "boolean"
    },
    "foreignAddressInd": {
      "type": "boolean"
    }
  }
}
*/

public class Address {
	public enum AddressTypeCode {
		Residence,Previous,Primary Address,Previous Residence Address,Previous Business Address,Policy Mailing Address,Mailing,Business,Billing Mailing
	}

	@Size(max=1)
	private String id;

	@Size(max=1)
	private com.digitalml.insurance.common.contact.Address.AddressTypeCode addressTypeCode;

	@Size(max=1)
	private String addressLine1;

	@Size(max=1)
	private String addressLine2;

	@Size(max=1)
	private String addressLine3;

	@Size(max=1)
	private String addressLine4;

	@Size(max=1)
	private String addressLine5;

	@Size(max=1)
	private String city;

	@Size(max=1)
	private String addressCountry;

	@Size(max=1)
	private String addressState;

	@Size(max=1)
	private String zip;

	@Size(max=1)
	private String prefAddressInd;

	@Size(max=1)
	private String startDate;

	@Size(max=1)
	private String endDate;

	@Size(max=1)
	private String lastUpdate;

	@Size(max=1)
	private String yearsAtAddress;

	@Size(max=1)
	private boolean returnedMailInd;

	@Size(max=1)
	private String postalDropCode;

	@Size(max=1)
	private boolean addressValidInd;

	@Size(max=1)
	private String countyName;

	@Size(max=1)
	private String countyCode;

	@Size(max=1)
	private boolean updateInd;

	@Size(max=1)
	private boolean uspsValidInd;

	@Size(max=1)
	private boolean foreignAddressInd;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    addressTypeCode = null;
	    addressLine1 = null;
	    addressLine2 = null;
	    addressLine3 = null;
	    addressLine4 = null;
	    addressLine5 = null;
	    city = null;
	    addressCountry = null;
	    addressState = null;
	    zip = null;
	    prefAddressInd = null;
	    startDate = null;
	    endDate = null;
	    lastUpdate = null;
	    yearsAtAddress = null;
	    
	    postalDropCode = null;
	    
	    countyName = null;
	    countyCode = null;
	    
	    
	    
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public com.digitalml.insurance.common.contact.Address.AddressTypeCode getAddressTypeCode() {
		return addressTypeCode;
	}
	
	public void setAddressTypeCode(com.digitalml.insurance.common.contact.Address.AddressTypeCode addressTypeCode) {
		this.addressTypeCode = addressTypeCode;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getAddressLine3() {
		return addressLine3;
	}
	
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}
	public String getAddressLine4() {
		return addressLine4;
	}
	
	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}
	public String getAddressLine5() {
		return addressLine5;
	}
	
	public void setAddressLine5(String addressLine5) {
		this.addressLine5 = addressLine5;
	}
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddressCountry() {
		return addressCountry;
	}
	
	public void setAddressCountry(String addressCountry) {
		this.addressCountry = addressCountry;
	}
	public String getAddressState() {
		return addressState;
	}
	
	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}
	public String getZip() {
		return zip;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPrefAddressInd() {
		return prefAddressInd;
	}
	
	public void setPrefAddressInd(String prefAddressInd) {
		this.prefAddressInd = prefAddressInd;
	}
	public String getStartDate() {
		return startDate;
	}
	
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getYearsAtAddress() {
		return yearsAtAddress;
	}
	
	public void setYearsAtAddress(String yearsAtAddress) {
		this.yearsAtAddress = yearsAtAddress;
	}
	public boolean getReturnedMailInd() {
		return returnedMailInd;
	}
	
	public void setReturnedMailInd(boolean returnedMailInd) {
		this.returnedMailInd = returnedMailInd;
	}
	public String getPostalDropCode() {
		return postalDropCode;
	}
	
	public void setPostalDropCode(String postalDropCode) {
		this.postalDropCode = postalDropCode;
	}
	public boolean getAddressValidInd() {
		return addressValidInd;
	}
	
	public void setAddressValidInd(boolean addressValidInd) {
		this.addressValidInd = addressValidInd;
	}
	public String getCountyName() {
		return countyName;
	}
	
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
	public String getCountyCode() {
		return countyCode;
	}
	
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public boolean getUpdateInd() {
		return updateInd;
	}
	
	public void setUpdateInd(boolean updateInd) {
		this.updateInd = updateInd;
	}
	public boolean getUspsValidInd() {
		return uspsValidInd;
	}
	
	public void setUspsValidInd(boolean uspsValidInd) {
		this.uspsValidInd = uspsValidInd;
	}
	public boolean getForeignAddressInd() {
		return foreignAddressInd;
	}
	
	public void setForeignAddressInd(boolean foreignAddressInd) {
		this.foreignAddressInd = foreignAddressInd;
	}
}