package com.digitalml.insurance.attachment;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Attachment:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "dateCreated": {
      "type": "string"
    },
    "userCode": {
      "type": "string"
    },
    "userName": {
      "description": "On Attachment, UserName provides a human readable representation of the user that corresponds to the UserCode.",
      "type": "string"
    },
    "attachmentSource": {
      "type": "string"
    },
    "description": {
      "description": "On Attachment, used to describe the type of attachment provided",
      "type": "string"
    },
    "lastUpdate": {
      "type": "string"
    },
    "attachmentType": {
      "type": "string"
    },
    "mimeType": {
      "type": "string"
    },
    "imageType": {
      "description": "On Attachment, ImageType provides a high level classification of the contents of the attachment. More specific details may be supplied in the MimeType property",
      "type": "string"
    },
    "attachmentLocation": {
      "description": "On Attachment, AttachmentLocation is used to specify which of the elements (AttachmentData, AttachmentReference, or AttachmentData64Binary) holds the contents of the Attachment",
      "type": "string"
    },
    "imageSubmissionType": {
      "description": "Image Submission Type\nOn Attachment, when AttachmentBasicType = 2 (Image), this identifies the location of the image. For example: if the image of a lab report is contained in the client file, this can be specified in ImageSubmissionType\n",
      "type": "string"
    },
    "priority": {
      "type": "string"
    },
    "fileName": {
      "type": "string"
    },
    "sequence": {
      "description": "On Attachment, Sequence describes the sequencing of Attachments when multiple Attachments need to be sorted for processing",
      "type": "string"
    },
    "receivedDate": {
      "description": "On Attachment, this is the date the attachment was received by the receiving system",
      "type": "string",
      "format": "date"
    },
    "pageCount": {
      "type": "string"
    }
  }
}
*/

public class Attachment {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String dateCreated;

	@Size(max=1)
	private String userCode;

	@Size(max=1)
	private String userName;

	@Size(max=1)
	private String attachmentSource;

	@Size(max=1)
	private String description;

	@Size(max=1)
	private String lastUpdate;

	@Size(max=1)
	private String attachmentType;

	@Size(max=1)
	private String mimeType;

	@Size(max=1)
	private String imageType;

	@Size(max=1)
	private String attachmentLocation;

	@Size(max=1)
	private String imageSubmissionType;

	@Size(max=1)
	private String priority;

	@Size(max=1)
	private String fileName;

	@Size(max=1)
	private String sequence;

	@Size(max=1)
	private Date receivedDate;

	@Size(max=1)
	private String pageCount;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    dateCreated = null;
	    userCode = null;
	    userName = null;
	    attachmentSource = null;
	    description = null;
	    lastUpdate = null;
	    attachmentType = null;
	    mimeType = null;
	    imageType = null;
	    attachmentLocation = null;
	    imageSubmissionType = null;
	    priority = null;
	    fileName = null;
	    sequence = null;
	    
	    pageCount = null;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getDateCreated() {
		return dateCreated;
	}
	
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getUserCode() {
		return userCode;
	}
	
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAttachmentSource() {
		return attachmentSource;
	}
	
	public void setAttachmentSource(String attachmentSource) {
		this.attachmentSource = attachmentSource;
	}
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getAttachmentType() {
		return attachmentType;
	}
	
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}
	public String getMimeType() {
		return mimeType;
	}
	
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public String getImageType() {
		return imageType;
	}
	
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	public String getAttachmentLocation() {
		return attachmentLocation;
	}
	
	public void setAttachmentLocation(String attachmentLocation) {
		this.attachmentLocation = attachmentLocation;
	}
	public String getImageSubmissionType() {
		return imageSubmissionType;
	}
	
	public void setImageSubmissionType(String imageSubmissionType) {
		this.imageSubmissionType = imageSubmissionType;
	}
	public String getPriority() {
		return priority;
	}
	
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getSequence() {
		return sequence;
	}
	
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public Date getReceivedDate() {
		return receivedDate;
	}
	
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	public String getPageCount() {
		return pageCount;
	}
	
	public void setPageCount(String pageCount) {
		this.pageCount = pageCount;
	}
}